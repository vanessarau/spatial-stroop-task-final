﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StroopGenerator : MonoBehaviour
{
    [Header("UI component")]
    public TextMeshProUGUI stroopLable;
    [Space]
    [Header("Stroop Components")]
    public List<Color32> stroopColors;
    public List<string> stroopText;
    [Range(0,100)]
    public float chanceOfCompatibility;
    
    private int _currentStroopColorIndex;
    private int _currentStroopTextIndex;
    
    private void Start()
    {
        _currentStroopColorIndex = 0;
    }

    public void GetNextStroopColor()
    {
        
        _currentStroopColorIndex = Mathf.FloorToInt(Random.Range(0, stroopColors.Count));
        stroopLable.color = stroopColors[_currentStroopColorIndex];
    }

    public void GetNextStroopText()
    {
        if (Random.Range(0, 100) < chanceOfCompatibility)
        {
            _currentStroopTextIndex = _currentStroopColorIndex;
        }
        else
        {
            do
            {
                _currentStroopTextIndex = Mathf.FloorToInt(Random.Range(0, stroopText.Count));    
            } while (_currentStroopTextIndex == _currentStroopColorIndex); 
        }
        stroopLable.text = stroopText[_currentStroopTextIndex];
    }
    
    /// <summary>
    /// get current shown stroop label color
    /// </summary>
    /// <returns>RGBA Unity Color32 variable</returns>
    public Color32 GetCurrectStroopColor()
    {
        return stroopColors[_currentStroopColorIndex];
    }

    public string GetCurrentStroopColorName()
    {
        return stroopText[_currentStroopColorIndex];
    }
    
    public string GetCurrentStroopText()
    {
        return stroopText[_currentStroopTextIndex];
    }

    public bool IsComplatible()
    {
        return (_currentStroopTextIndex == _currentStroopColorIndex);
    }
}
