﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using UnityEditor;
using UnityEngine;
using Valve.VR;
using Color = UnityEngine.Color;

public class SpatialExperimentManager : MonoBehaviour
{
    
    [Header("Experiment components")]
    public SpatialStroopGenerator stroopGenerator;
    
    public LableController lableController;
    [Header("Block variables")] 
    public int numberOfBlocks;
    public int numberOfTasks;

    private int _currentTaskNumber;
    private int _currentBlockNumber;

    private float _startStimuliTime;

    private float _responseTime;
    
    public SteamVR_Action_Boolean answereYes;
    public SteamVR_Action_Boolean answereNo;
    public SteamVR_Action_Boolean space;

    private bool _welcomeRead;
    private bool _waitingForNextTask;
    
    // string array for messages
    public String[] messages;
    
    // To check if we are already in a block 
    private bool _isBlockRunning;
    
    // data to be written into the CSV file
    private List<List<string>> _data;
    
    // the headers that will be used for the result CSV file at the end
    private List<string> _csvHeaders;

    // The canvasObject for text messages
    public GameObject CanvasObject;
    
    public GameObject FixationCross;
    
    // the VR headset to get level of eye hight
    public Camera VrCam;



    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        

        // Code to execute after the delay
    }
    private void Start()
    {
        
        // Introduction
        ShowMessage(Color.white, $"Welcome! In this experiment we elaborate modified behaviour. \n Your task is to decide if the shown direction by an arrow and its position match. \n Press left trigger for yes and right trigger for no. \n Press menu to continue", false);
        
        // set fixation cross and text instructions at eye level
        FixationCross.transform.position = new Vector3(FixationCross.transform.position.x, VrCam.transform.position.y, FixationCross.transform.position.z);
        CanvasObject.transform.position = new Vector3(CanvasObject.transform.position.x, FixationCross.transform.position.y, FixationCross.transform.position.z);
        
        
        _currentTaskNumber = 0;
        _currentBlockNumber = 0;
        _data = new List<List<string>>();
        _csvHeaders = new List<string>();
        _csvHeaders.Add("Block Number");
        _csvHeaders.Add("Task Number");
        _csvHeaders.Add("Shown Arrow Location");
        _csvHeaders.Add("Shown Arrow Rotation");
        _csvHeaders.Add("Was Compatible");
        _csvHeaders.Add("Answered Correctly");
        _csvHeaders.Add("Reaction Time");
        
        // At the beginning we are not running a block yet
        _isBlockRunning = false;
        _waitingForNextTask = false;
        
        stroopGenerator.CreateLocationDictionary(FixationCross.transform);
    }

    // create particapants experiment output data
    private void ParticipantAnswered(bool participantAnswer)
    {
        if (!_waitingForNextTask)
        {
        List<string> taskData = new List<string>();
        _responseTime = Time.realtimeSinceStartup;
        
        // add the task (trial) data to the list
        taskData.Add(_currentBlockNumber.ToString());
        taskData.Add(_currentTaskNumber.ToString());
        taskData.Add(stroopGenerator.GetArrowLocation());
        taskData.Add(stroopGenerator.GetArrowRotation());
        taskData.Add(stroopGenerator.IsCompatible().ToString());
        taskData.Add((stroopGenerator.IsCompatible() == participantAnswer).ToString());
        
        // reaction time is simply response time minus the time at which the stimulus was started
        taskData.Add((_responseTime - _startStimuliTime).ToString());
        
        // add task (trial) data to the list that will later be written into the csv file
        _data.Add(taskData);
        
        
        StartCoroutine(GetNextTask());
        }
    }
    
    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            CanvasObject.SetActive(false);
        }

        // is the block running or not
        if (_isBlockRunning && _welcomeRead)
        {
           
            if (answereYes.GetStateDown(SteamVR_Input_Sources.Any))
            {
            
                ParticipantAnswered(true);
            }
            if (answereNo.GetStateDown(SteamVR_Input_Sources.Any))
            {
                ParticipantAnswered(false);
            }
            
            // Participant said yes ( color matches the text)
            if (Input.GetKeyDown(KeyCode.Y))
            {
                ParticipantAnswered(true);
            }
            
            // participant said no ( color doesn't match the text)
            else if(Input.GetKeyDown(KeyCode.N))
            {
                ParticipantAnswered(false);
            }
        }
        else if (_welcomeRead)
        {
            if (space.GetStateDown(SteamVR_Input_Sources.Any))
            {
                StartNextBlock();
            }
            
            // if we are between two blocks we start next block when space is pressed
            if (Input.GetKeyDown(KeyCode.Space))
            {
                StartNextBlock();
            }
        }
        else
        {
            if (space.GetStateDown(SteamVR_Input_Sources.Any))
            {
                _welcomeRead = true;
                ShowMessage(Color.white, "To start the experiment, fixate the cross in the center and press menu to continue");
            }
        } 
    }

    private IEnumerator GetNextTask()
    {
       // check if there are any tasks (trials) left in the block
       _waitingForNextTask = true;
       stroopGenerator.Arrow.SetActive(false);
       yield return new WaitForSecondsRealtime(1f);
       _waitingForNextTask = false;
       
       stroopGenerator.Arrow.SetActive(true);
        if (_currentTaskNumber < numberOfTasks)
        {
            
            // continue with the next task (trial)
            stroopGenerator.GetNextArrow();
            // set stimulus onset time to the current runtime of the program
            _startStimuliTime = Time.realtimeSinceStartup;
            _currentTaskNumber++;
        }
        
        // if the block is finished
        else
        {
            
            stroopGenerator.Arrow.SetActive(false);
            // check if there are any blocks left
            if (_currentBlockNumber <= numberOfBlocks)
                // prompt input from user
            {
                
                CanvasObject.SetActive(true);
                
                ShowMessage(Color.white,"End of block, press menu to continue");
               
            }
            
            _isBlockRunning = false;
        }

        
    }

    private void StartNextBlock()
    {
       // if there is a trial block left, start it 
        if (_currentBlockNumber < numberOfBlocks)
        {
            ShowMessage(Color.white, "");
            ResetTaskNumber();
            _isBlockRunning = true;
            _currentBlockNumber++;

            if (!_waitingForNextTask)
            {
            StartCoroutine(GetNextTask());
                
            }
        }
        
        // else end the experiment
        else
        {
            CanvasObject.SetActive(true);
            ShowMessage(Color.magenta,"End of experiment, thanks for participating!", false);
            
            // write everything into the result CSV file
            List<string> csvLines = CSVTools.GenerateCSV(_data, _csvHeaders);
            foreach (string line in csvLines)
            {
                Debug.Log(line);
            }
            // Is the usage of a GUID useful? Maybe a simple string, participant number or something like
            // that is more appropriate.
            CSVTools.SaveCSV(csvLines, Application.dataPath + "/Data/" + DateTime.Now.ToString("yyyyMMddHHmmss"));
        }
    }

    // show instructions msg to participants in the color32 color
    //show fixation cross at all times
    private void ShowMessage(Color32 color, string msg, bool showCross = true)
    {
        lableController.lableColor = color;
        lableController.ShowText(msg, showCross);
    }
    
    private void ResetTaskNumber()
    {
        _currentTaskNumber = 0;
    }

}
